import React from 'react';
import ReactDOM from 'react-dom';

let quote = {
    text: 'Le pessimiste est celui qui, entre deux maux, choisit les deux.',
    author: 'Oscar Wilde'
};


let quotes = [
    {
        text: 'Le pessimiste est celui qui, entre deux maux, choisit les deux.',
        author: 'Oscar Wilde'
    },
    {
        text: 'Entre deux maux, choisit les deux.',
        author: 'Louison Chevalier'
    },
    {
        text: 'Le pessimiste est celui qui, entre deux maux, choisit les deux.',
        author: 'Test Test'
    }
];


class Quotation extends React.Component {
    render() {
        const q = this.props.quote
        return (
            <div className="well">
                <div>
                    <h1 className="text-center">{q.author} </h1>
                    <p className="text-center">{q.text} </p>
                </div>
            </div>
        );
    }
}

class Test extends React.Component{
    constructor(props) {
        super(props);
        this.w = this.w.blind(this);
    }
    w(){
        console.log("Test");
    }
    render(){
        return(
            <button className="btn btn-primary" onClick={this.w} >Test</button>
        );
    }
}


class Root extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            quotes: quotes,
            currIndex: 0,
            title : "ES6/React"
        }
        this.incrementIndex=this.incrementIndex.bind(this);
        this.decrementIndex=this.decrementIndex.bind(this);
    }

    incrementIndex(){
        const newIndex= this.state.currIndex +1;
        if (newIndex < this.state.quotes.length){
            this.setState({currIndex: newIndex})
        }
    }

    decrementIndex(){
        if (this.state.currIndex != 0 ){
            const newIndex= this.state.currIndex -1
            this.setState({currIndex: newIndex})
        }
    }

    render() {
        const q = this.state.quotes[this.state.currIndex];
        const rule = { fontSize: "3.5rem" };
        return (
                <div className="container">
                    <hr/>
                    <h2 className="text-center" style={rule}>{this.state.title}</h2>
                    <hr/>
                    <Quotation quote={q} />
                    <div className="text-center">
                        <button onClick={this.decrementIndex} className="btn btn-primary" >Previous</button>
                        <button onClick={this.incrementIndex} className="btn btn-primary">Next</button>
                    </div>
                </div>

        );
    }
}

ReactDOM.render(<Root />, document.getElementById('root'));